# -*- coding: utf-8 -*-
"""
Created on Tue Oct  2 17:01:29 2018

@author: Clement
"""
#
# Documentation for solve_ODEs_CA3.py
# This function defines a system of ordinary differential equations used in chemistry
# These equations describe the molar flow rate of the chimical componant A, B, C and the temperature variation according to the volume.
# The 4 diferential equations studies are :
#
#$\frac{dF_A}{dV}=r_A$
#$\frac{dF_B}{dV}=r_B$
#$\frac{dF_C}{dV}=r_C$
#$\frac{dT}{dV}=\frac{U_a\left(T_a-T\right)+\left(-r_{1A}\right)\left(-\Delta H_{Rx1A}\right)+\left(-r_{2A}\right)\left(-\Delta H_{Rx2A}\right)}{F_AC_{P_A}+F_BC_{P_B}+F_CC_{P_C}}$
#
# The parameters present in these equations are compute with these aditional equations :
#
#$r_A=r_{1A}+r_{2A}=-k_{1A}-k_{2A}C_A^2$
#$r_B=r_{1B}=k_{1A}C_A$
#$r_c=r_{2c}=\frac{1}{2}k_{2A}C_A^2$
#
# In the following equation, i can be remplace by A,B or C. The same letter should be used at ont time.
# Here is a list of the major variables used with the initial values, the units and the meaning.
#Ca = 0.1					Latent heat for the body a				[J/g]		
#Cb = 0					Latent heat for the body b				[J/g]
#Cc = 0					Latent heat for the body c				[J/g]
#CT0 = 0.1					Latent heat 							[J/g]
#FA = 100					molar flow rate for the species A		[mol/s]
#FB = 0					molar flow rate for the species B		[mol/s]
#FC = 0					molar flow rate for the species C		[mol/s]
#FT = 100					molar flow rate 						[mol/s]
#k1A = 482.8247 											% Units [s^(-1)]
#k2A = 553.0557			 								% Units [dm^3/(mol.s)]
#r1a = -4828247			Relative rate for the reaction 1
#r2a = -5.530557			Relative rate for the reaction 2
#T							Temperature								[K]
#T0 = 423					Temperature at begining					[K]
#V = 0						Volume									[dm^3]
#Ua = 4000 												 %Units [J/(m^3*s)]
#Ta = 100					(Constant)						[°C]
#DeltaH_Rx1A = -20,000		Latent heat		[J/(mol of A reacted in reaction 1)]
#DeltaH_Rx2A = -60,000		Latent heat 	[J/(mol of A reacted in reaction 2)]
#CPA = 90						% Units [J/(mol*°C)]
#CPB = 90						% Units [J/(mol*°C)]
#CPC = 90						% Units [J/(mol*°C)]
#
# Input :
# The values initiales and the constantes should be enter before running the script.
#
# Output :
#T he script will compute the the differential equations and plot the result at the end
# The result are available in a vector named "F". The first column is FA, the second is FB, the third is FC end the last is T.
# To acces to a column after have executed the programe once, write "F(:,i)" with i the number of the colum.
#
# This code has been written with Spyder
# Author:
#			Student Clement Brousse   
#  		    Oklahoma State University, Department of Mechanical and Aerospace Engineering (MAE)
#  		    clbrous@okstate.edu
# Last revision : 10/3/2018
#



import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# Define initiale values
k1A = 482.8247              # Units [s^(-1)]k2A = 553.0557              # Units [dm^3/(mol.s)]
CT0 = 0.1
T0 = 423                    # Units [K]
Ua = 4000                   # Units [J/(m^3*s)]
Ta = 100                    # Units [°C]
DeltaH_Rx1A = -20000        # Units [J/(mol of A reacted in reaction 1)]
DeltaH_Rx2A = -60000        # Units [J/(mol of A reacted in reaction 2)]
CPA = 90                    # Units [J/(mol*°C)]
CPB = 90                    # Units [J/(mol*°C)]
CPC = 180                   # Units [J/(mol*°C)]
FA = 100                    # Units [mol/s]
FB = 0                      # Units [mol/s]
FC = 0                      # Units [mol/s]
T = 100                     # Units [mol/s]
Ca = 0.1
Cb = 0
Cc = 0
Ct0 = 0.1
Ft = 100
r1a = -48.28247
r2a = -5.530557
V = 0  
   

def ODEs_CA3(k1A,Ct0,FA,FB,FC,T0,T,k2A,Ua,Ta,DeltaH_Rx1A,DeltaH_Rx2A,CPA,CPB,CPC) :
    # Define initiale values
    k1A = 482.8247              # Units [s^(-1)]
    k2A = 553.0557              # Units [dm^3/(mol.s)]
    CT0 = 0.1
    T0 = 423                    # Units [K]
    Ua = 4000                   # Units [J/(m^3*s)]
    Ta = 100                    # Units [°C]
    DeltaH_Rx1A = -20000        # Units [J/(mol of A reacted in reaction 1)]
    DeltaH_Rx2A = -60000        # Units [J/(mol of A reacted in reaction 2)]
    CPA = 90                    # Units [J/(mol*°C)]
    CPB = 90                    # Units [J/(mol*°C)]
    CPC = 180                   # Units [J/(mol*°C)]
    FA = 100                    # Units [mol/s]
    FB = 0                      # Units [mol/s]
    FC = 0                      # Units [mol/s]
    T = 100                     # Units [mol/s]
    Ca = 0.1
    Cb = 0
    Cc = 0
    Ct0 = 0.1
    Ft = 100
    r1a = -48.28247
    r2a = -5.530557
    V = 0  
   
    # Define the derivative
    dFA_dV = (-k1A)*CT0*(FA/(FA+FB+FC))*(T0/T)
    dFB_dV = k1A*CT0*(FA/(FA+FB+FC))*(T0/T)
    dFC_dV = 0.5*k2A*CT0*(FA/(FA+FB+FC))*(T0/T)
    dT_dV  = (Ua*(Ta-T)+(k1A*CT0*(FA/(FA+FB+FC))*(T0/T))*(-DeltaH_Rx1A)+k2A*pow(CT0,2)*pow(FA/(FA+FB+FC),2)*pow((T0/T),2)*(-DeltaH_Rx2A))/(90*(CPA+CPB)+180*CPC)	

    return dFA_dV, dFB_dV, dFC_dV, dT_dV
# Defining the x range and granularity
xrange = np.linspace(0, 1, 100)

# Defining the parameters values
parameters = (k1A,Ct0,FA,FB,FC,T0,T,k2A,Ua,Ta,DeltaH_Rx1A,DeltaH_Rx2A,CPA)  #,CPB,CPC

# Defining initial values
FA0 = 100 
FB0 = 0
FC0 = 0
FD0 = 100
initial_val = FA0, FB0, FC0, FD0

# Calling odeint to solve ODEs
output = odeint (ODEs_CA3, initial_val, xrange, args = parameters)
print("End of odeint")


# output vectors of numerators for each value of the denominator in the xrange
FA = output[:,0]
FB = output[:,1]
FC = output[:,2]
T  = output[:,3]


# Plotting
plt.plot(xrange, FA, 'r--', xrange, FB, 'bs', xrange, FC, 'g^')
plt.title("$F_A, F_B, F_C$ vs. V")
plt.legend(['$F_A$', '$F_B$', '$F_C$'])
plt.ylabel('$F_A$, $F_B$, $F_C$')
plt.xlabel('V [$dm^3]$')
plt.show()



plt.plot(xrange, T)
plt.title("$T$ vs. V")
plt.legend(['$T$'])
plt.ylabel('$T$')
plt.xlabel('V [$dm^3]$')
plt.show()

