%% Presentation of the code
% This code has been written by Clement Brousse as a computational assignement in the OSU
% 
% The program need to enter a percentage of attraction between bees and the length of the simulation.
% The program will show the simuation result after few seconds needed to compute.
% 
% If you find a mistake, please contact the author.
% To contact the author : clbrous@okstate.edu.
% Last review : 11/13/2018

%% Code
function varargout = BeeAttraction(varargin)
% BEEATTRACTION MATLAB code for BeeAttraction.fig
%      BEEATTRACTION, by itself, creates a new BEEATTRACTION or raises the existing
%      singleton*.
%
%      H = BEEATTRACTION returns the handle to a new BEEATTRACTION or the handle to
%      the existing singleton*.
%
%      BEEATTRACTION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BEEATTRACTION.M with the given input arguments.
%
%      BEEATTRACTION('Property','Value',...) creates a new BEEATTRACTION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before BeeAttraction_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to BeeAttraction_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help BeeAttraction

% Last Modified by GUIDE v2.5 13-Nov-2018 17:47:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @BeeAttraction_OpeningFcn, ...
                   'gui_OutputFcn',  @BeeAttraction_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before BeeAttraction is made visible.
function BeeAttraction_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to BeeAttraction (see VARARGIN)

% Choose default command line output for BeeAttraction
handles.output = hObject;

% Display the bee picture
% Question 4
BeeImage = imread('Beeimage.jpg');
axes(handles.axes1);
imshow(BeeImage)

% Code add in the question 6
handles.attraction = 0.0; %no attraction
handles.totalTimePoints = 150;% number of time points
handles.output = hObject;
addpath(genpath('beefiles'))
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes BeeAttraction wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = BeeAttraction_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.attraction = str2double(get(hObject,'string'))/100;
guidata(hObject, handles);


% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% This is the question 12
axes(handles.axes2);
simulation_attraction(hObject, eventdata, handles)
handles.totalTimePoints;
handles.attraction;
guidata(hObject, handles);


% --- Executes on selection change in listbox1.
% function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% 
% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1
% 
% 
% --- Executes during object creation, after setting all properties.
% function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% 
% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
% if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
%     set(hObject,'BackgroundColor','white');
% end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% This is the question 10
if get(handles.popupmenu1,'value') == 1
     handles.totalTimePoints = 150;
elseif get(handles.popupmenu1,'value') == 2
     handles.totalTimePoints = 300;
elseif get(handles.popupmenu1,'value') == 3
     handles.totalTimePoints = 1500;
end
guidata(hObject, handles);

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on key press with focus on pushbutton1 and none of its controls.
function pushbutton1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
