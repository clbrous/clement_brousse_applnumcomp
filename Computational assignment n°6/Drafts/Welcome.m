function varargout = Welcome(varargin)
% WELCOME MATLAB code for Welcome.fig
%      WELCOME, by itself, creates a new WELCOME or raises the existing
%      singleton*.
%
%      H = WELCOME returns the handle to a new WELCOME or the handle to
%      the existing singleton*.
%
%      WELCOME('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WELCOME.M with the given input arguments.
%
%      WELCOME('Property','Value',...) creates a new WELCOME or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Welcome_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Welcome_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Welcome

% Last Modified by GUIDE v2.5 06-Dec-2018 16:39:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Welcome_OpeningFcn, ...
                   'gui_OutputFcn',  @Welcome_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Welcome is made visible.
function Welcome_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Welcome (see VARARGIN)

% Choose default command line output for Welcome
handles.numODEs = 0;
guidata(hObject, handles);


% UIWAIT makes Welcome wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Welcome_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% while a == 0 % Check all 3s to reopen the gui. Loop without end.
% uiwait(Input_equations())
% disp('Hey I am in the loop');
% if i == 1
%     open(Input_equations())
%     i = 0;
% elseif i == 2
%     open(Input_simple_equations())
%     i = 0;
% else
% end


% Get default command line output from handles structure
% varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Input_equations();

handles.numODEs = handles.numODEs +1
uiwait(Input_equations())
Load_variables(handles.numODEs)

guidata(hObject, handles);

% close('Welcome')


% --- Executes on button press in Add_simple_equation.
function Add_simple_equation_Callback(hObject, eventdata, handles)
% hObject    handle to Add_simple_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Input_simple_equations()

handles.numODEs = handles.numODEs +1
numODEs = handles.numODEs;
uiwait(Input_simple_equations())
Load_variables(numODEs)
guidata(hObject, handles);

% --- Executes on button press in pushbutton_compute_results.
function pushbutton_compute_results_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_compute_results (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

numODEs = handles.numODEs;
[Numerator{numODEs}, Denominator{numODEs}, Right_side{numODEs}, Comments{numODEs}, Equation{numODEs}, Initial_value{numODEs}] = Load_variables(handles.numODEs);
handles.Initial_value = Initial_value;
handles.Equation = Equation;
handles.Comments = Comments;
handles.Right_side = Right_side;
handles.Denominator = Denominator;
handles.Numerator = Numerator;

% [Numerator, Denominator, Right_side, Comments, Equation, Initial_value] = parameters;
numODEs = handles.numODEs;
y_output = Solver(Numerator, Denominator, Right_side, Equation, Initial_value,numODEs);
disp('Equation(s) solve');
handles.solution = y_output;
% disp(y_output)
disp(handles.solution)
guidata(hObject, handles);


% --- Executes on button press in pushbutton_plot_results.
function pushbutton_plot_results_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_plot_results (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
y_output = handles.solution
Numerator = handles.Numerator;
disp(handles)
numODEs = handles.numODEs;
fig = plot_chart(y_output,Numerator,numODEs)
handles.fig = fig;
% disp(fig)
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes2

 

function [Numerator, Denominator, Right_side, Comments, Equation, Initial_value] = Load_variables(numODEs)

A= struct2cell(load('Initial_value.mat', '-mat'));
disp(A) 
B = cellstr(A);
C = cell2mat(B);
Initial_value = str2num(C);
handles.Initial_value {numODEs} = Initial_value;
% Initial_value(k) = Initial_value;
disp('all field');
disp (getfield(handles,'Initial_value'));

A= struct2cell(load('Numerator.mat', 'Numerator')); 
B = cellstr(A);
C = cell2mat(B);
% Numerator = str2num(C)
Numerator = B;
handles.Numerator {numODEs} = Numerator;

% Numerator (k) = Numerator;

A= struct2cell(load('Denominator.mat', 'Denominator'));
B = cellstr(A);
C = cell2mat(B);
Denominator = B
% Denominator = str2num(C)
handles.Denominator {numODEs} = Denominator;
% Denominator (k) = Denominator;

A= struct2cell(load('Right_side.mat', 'Right_side'));
B = cellstr(A);
C = cell2mat(B);
Right_side = B
% Right_side = str2num(C)
handles.Right_side {numODEs} = Right_side;
% Right_side (k) = Right_side;

A= struct2cell(load('Comments.mat', 'Comments'));
B = cellstr(A);
C = cell2mat(B);
Comments = B
% Comments = str2num(C)
handles.Comments {numODEs} = Comments;
% Comments (k) = Comments;

A= struct2cell(load('Equation.mat', 'Equation'));
B = cellstr(A);
C = cell2mat(B);
Equation = B
Equation = str2num(C)
handles.Equation {numODEs} = Equation;

% disp('Show all values');
% for i=1:handles.numODEs
%     disp(['i= ', num2str(i) ]);
%     disp(['Numerator = ', Numerator{i}]);
%     disp(['Denominator = ', Denominator{i}]);
%     disp(['Right_side = ', Right_side{i}]);
%     disp(['Denominator = ', Denominator{i}]);
%     disp(['Comments = ', Comments{i}]);
%     disp(['Equation = ', Equation{i}]);
% end






%% Solve the equations

%Define the equations
    function dydt = ODE(t, y, Numerator, Denominator, Right_side, Equation)
        % Input: t and y are the independent and dependent variable values
        % denominators, numerators, RHSs, and explicitEqns are cell
        % arrays with the first three terms defining the ODEs of the form
        % d(numerators) / d(denominators) = RHSs and the fourth term
        % defining the associated explicit equations
        % Output: the derivative vector dy/dt for y(1):y(numODEs) where
        % numODEs = length(numerators) = length(RHSs) = length(denominators)
        % independent variable
        str0=cell2mat(Denominator(1));
        eval(strcat(str0,'=t;'));
        % dependent variables
        for i = 1:length(Numerator)
            str1 = cell2mat(Numerator(i));
            eval(strcat(str1,'=y(i);'));
        end
        % explicit equations provided in MATLAB acceptable order, can be semicolon separated list
        for i = 1:length(Equation)
            str2 = cell2mat(Equation(i));
            eval(str2);
        end
        % Right-hand sides of ODE definitions
        for i = 1:length(Right_side)
            str3 = cell2mat(Right_side(i));
            dydt(i) = eval(str3);
        end

dydt = dydt';


%Solve the equations
%% Define the solution
%         function y_output = Solver(Numerator, Denominator, Right_side, Equation, Initial_value)
%             %         for i = 1:length(tdata);
%             %             tspan = linspace(0,tdata(i),101);
%             tspan = linspace(0, 100);
%             [~,y_calc] = ode23s(@(t,y) ODE(t, y, Numerator, Denominator, Right_side, Equation),tspan, Initial_value);
%             y_output = y_calc;
%             %          end
%             y_output = y_output';

        function y_output = Solver(Numerator, Denominator, Right_side, Equation, Initial_value,numODEs)
            for i = 1:numODEs;
                tspan = linspace(0, 100);
                [~,y_calc] = ode45(@(t,y) ODE(t, y, Numerator, Denominator, Right_side, Equation),tspan, Initial_value);
                y_output(i,:)=y_calc;
%                disp(y_output)
               disp(['i = ', num2str(i)])
            end
            
%% Define the function to plot the graph
            function fig = plot_chart(y_output,Numerator,numODEs)
                figure (1)
                leg = '';
                hold on
%                 set(handles.axes2)
%                 axe(handles.axes2)
                tspan = linspace(0, 100);
                y_output;
                for i = 1:numODEs
                    y_output(:,i)
                plot (tspan, y_output(i, :));
                leg {end+1} = ['Equation n� ', num2str(i)]; 
                end
                legend(leg)
                title('Computational assignement n�6')
                
                hold off
                fig = figure(1);
                fig;

