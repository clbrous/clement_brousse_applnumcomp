function varargout = Input_equations(varargin)
% INPUT_EQUATIONS MATLAB code for Input_equations.fig
%      INPUT_EQUATIONS, by itself, creates a new INPUT_EQUATIONS or raises the existing
%      singleton*.
%
%      H = INPUT_EQUATIONS returns the handle to a new INPUT_EQUATIONS or the handle to
%      the existing singleton*.
%
%      INPUT_EQUATIONS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INPUT_EQUATIONS.M with the given input arguments.
%
%      INPUT_EQUATIONS('Property','Value',...) creates a new INPUT_EQUATIONS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Input_equations_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Input_equations_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Input_equations

% Last Modified by GUIDE v2.5 04-Dec-2018 12:55:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Input_equations_OpeningFcn, ...
                   'gui_OutputFcn',  @Input_equations_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Input_equations is made visible.
function Input_equations_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Input_equations (see VARARGIN)

% Choose default command line output for Input_equations
handles.output = hObject;
load('Temporary_Variables.mat', 'k')
k = 0;
k = k+1;
handles.k = k;
save('Temporary_Variables.mat', 'k')
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Input_equations wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Input_equations_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function Numerator_Callback(hObject, eventdata, handles)
% hObject    handle to Numerator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Numerator = get(hObject,'string')

% handles.Numerator = str2double(get(hObject,'string'))
% Numerator (handles.k) = handles.Numerator
% save('Temporary_Variables.mat', 'Numerator')
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Numerator as text
%        str2double(get(hObject,'String')) returns contents of Numerator as a double


% --- Executes during object creation, after setting all properties.
function Numerator_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Numerator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Denominator_Callback(hObject, eventdata, handles)
% hObject    handle to Denominator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Denominator = get(hObject,'string')

% handles.Denominator = str2double(get(hObject,'string'))
% Denominator(handles.k) = handles.Denominator
% save('Temporary_Variables.mat', 'Denominator')
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Denominator as text
%        str2double(get(hObject,'String')) returns contents of Denominator as a double


% --- Executes during object creation, after setting all properties.
function Denominator_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Denominator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Right_side_Callback(hObject, eventdata, handles)
% hObject    handle to Right_side (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Right_side = get(hObject,'string');

% handles.Right_side = str2double(get(hObject,'string'))
% Right_side (handles.k) = handles.Right_side;
% save('Temporary_Variables.mat', 'Right_side')
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Right_side as text
%        str2double(get(hObject,'String')) returns contents of Right_side as a double


% --- Executes during object creation, after setting all properties.
function Right_side_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Right_side (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Initial_value_Callback(hObject, eventdata, handles)
% hObject    handle to Initial_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Initial_value = get(hObject,'string');


% handles.Initial_value = str2double(get(hObject,'string'))
Initial_value(handles.k) = handles.Initial_value;
% save('Temporary_Variables.mat', 'Initial_value')
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Initial_value as text
%        str2double(get(hObject,'String')) returns contents of Initial_value as a double


% --- Executes during object creation, after setting all properties.
function Initial_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Initial_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Comments_Callback(hObject, eventdata, handles)
% hObject    handle to Comments (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Comments = get(hObject,'string')
% Comments(handles.k) = handles.Comments
% save('Temporary_Variables.mat', 'Comments')
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Comments as text
%        str2double(get(hObject,'String')) returns contents of Comments as a double


% --- Executes during object creation, after setting all properties.
function Comments_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Comments (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_new_equation.
function pushbutton_new_equation_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_new_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Save the results
Initial_value = handles.Initial_value;
Numerator = handles.Numerator;
Denominator = handles.Denominator;
Right_side = handles.Right_side;
Comments = handles.Comments;

Save_values(Numerator, Denominator, Right_side, Initial_value, Comments)
guidata(hObject, handles);
close(Input_equations)

% --- Executes on button press in pushbutton_done.
function pushbutton_done_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_done (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Save the results
Initial_value = '';
Numerator = '';
Denominator = '';
Right_side = '';
Comments = '';
Save_values(Numerator, Denominator, Right_side, Initial_value, Comments)
guidata(hObject, handles);
close(Input_equations)


% --- Executes on button press in New_simple_equation.
function New_simple_equation_Callback(hObject, eventdata, handles)
% hObject    handle to New_simple_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Save the results
Initial_value = handles.Initial_value;
Numerator = handles.Numerator;
Denominator = handles.Denominator;
Right_side = handles.Right_side;
Comments = handles.Comments;
Save_values(Numerator, Denominator, Right_side, Initial_value, Comments)
guidata(hObject, handles);
close(Input_equations)



function Save_values(Numerator, Denominator, Right_side, Initial_value, Comments)
% Save the initial value
% Initial_value = handles.Initial_value
% load('Initial_value.mat', 'Initial_value')
Initial_value = string(Initial_value);
if ~isfile('Initial_value.mat')  % Check if the file existe
     save('Initial_value.mat', 'Initial_value') % Create the file the first launch
else
    save('Initial_value.mat', 'Initial_value', '-append') % Add to the file the others launch
end

% Save the Numerator
% Numerator = handles.Numerator
Numerator = string(Numerator);
if ~isfile('Numerator.mat')  % Check if the file existe
     save('Numerator.mat', 'Numerator') % Create the file the first launch
else
    save('Numerator.mat', 'Numerator', '-append') % Add to the file the others launch
end

% Save the Denominator
% Denominator = handles.Denominator
Denominator =  string(Denominator);
if ~isfile('Denominator.mat')  % Check if the file existe
     save('Denominator.mat', 'Denominator') % Create the file the first launch
else
    save('Denominator.mat', 'Denominator', '-append') % Add to the file the others launch
end

% Save the Right_side
% Right_side = handles.Right_side
Right_side = string(Right_side);
if ~isfile('Right_side.mat')  % Check if the file existe
     save('Right_side.mat', 'Right_side') % Create the file the first launch
else
    save('Right_side.mat', 'Right_side', '-append') % Add to the file the others launch
end

% Save the Comments
% Comments = handles.Comments
% disp(string(Comments)) % For debug only
Comments = string(Comments);
if ~isfile('Comments.mat')  % Check if the file existe
     save('Comments.mat', 'Comments') % Create the file the first launch
else
    save('Comments.mat', 'Comments', '-append') % Add to the file the others launch
end

% save('Variables.mat', 'Numerator', 'Denominator', 'Right_side', 'Comments', 'Initial_value')
% save Variables.mat Numerator Denominator Right_side Comments Initial_value
disp('Values saved')
