function varargout = Input_simple_equations(varargin)
% INPUT_SIMPLE_EQUATIONS MATLAB code for Input_simple_equations.fig
%      INPUT_SIMPLE_EQUATIONS, by itself, creates a new INPUT_SIMPLE_EQUATIONS or raises the existing
%      singleton*.
%
%      H = INPUT_SIMPLE_EQUATIONS returns the handle to a new INPUT_SIMPLE_EQUATIONS or the handle to
%      the existing singleton*.
%
%      INPUT_SIMPLE_EQUATIONS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INPUT_SIMPLE_EQUATIONS.M with the given input arguments.
%
%      INPUT_SIMPLE_EQUATIONS('Property','Value',...) creates a new INPUT_SIMPLE_EQUATIONS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Input_simple_equations_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Input_simple_equations_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Input_simple_equations

% Last Modified by GUIDE v2.5 04-Dec-2018 13:00:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Input_simple_equations_OpeningFcn, ...
                   'gui_OutputFcn',  @Input_simple_equations_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Input_simple_equations is made visible.
function Input_simple_equations_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Input_simple_equations (see VARARGIN)

% Choose default command line output for Input_simple_equations
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Input_simple_equations wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Input_simple_equations_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function Input_simple_equation_Callback(hObject, eventdata, handles)
% hObject    handle to Input_simple_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Equation = get(hObject,'string')
guidata(hObject, handles);

% Hints: get(hObject,'String') returns contents of Input_simple_equation as text
%        str2double(get(hObject,'String')) returns contents of Input_simple_equation as a double


% --- Executes during object creation, after setting all properties.
function Input_simple_equation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Input_simple_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Add_new_ODE.
function Add_new_ODE_Callback(hObject, eventdata, handles)
% hObject    handle to Add_new_ODE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Equation = get(hObject,'string');
Equation = handles.Equation;
Save_values(Equation,i);
guidata(hObject, handles);
close(Input_simple_equations())


% --- Executes on button press in Add_new_simple_equation.
function Add_new_simple_equation_Callback(hObject, eventdata, handles)
% hObject    handle to Add_new_simple_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Equation = get(hObject,'string');
Equation = handles.Equation;
i = 0;
Save_values(Equation);
guidata(hObject, handles);
close(Input_simple_equations())

% --- Executes on button press in Push_button_done.
function Push_button_done_Callback(hObject, eventdata, handles)
% hObject    handle to Push_button_done (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Equation = get(hObject,'string');
Equation = handles.Equation;
i = 0;
Save_values(Equation);
guidata(hObject, handles);
close(Input_simple_equations())


function Save_values(Equation)
% Save the initial value
% Initial_value = handles.Initial_value
Equation = string(Equation);
if ~isfile('Equation.mat')  % Check if the file existe
     save('Equation.mat', 'Equation') % Create the file the first launch
else
    save('Equation.mat', 'Equation', '-append', '-v7.3') % Add to the file the others launch
end

% If i = 1 -> add new ODE
% If i = 2 -> add new simple equation
% If i = 3 -> add new done
% Save the Comments
i = 1;
% disp(string(Comments)) % For debug only
if ~isfile('Order_i.mat')  % Check if the file existe
     save('Order_i.mat', 'i') % Create the file the first launch
else
    save('Order_i.mat', 'i', '-append', '-v7.3') % Add to the file the others launch
end

disp('Values saved')
