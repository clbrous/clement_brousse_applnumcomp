function varargout = Main_GUI(varargin)
% MAIN_GUI MATLAB code for Main_GUI.fig
%      MAIN_GUI, by itself, creates a new MAIN_GUI or raises the existing
%      singleton*.
%
%      H = MAIN_GUI returns the handle to a new MAIN_GUI or the handle to
%      the existing singleton*.
%
%      MAIN_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN_GUI.M with the given input arguments.
%
%      MAIN_GUI('Property','Value',...) creates a new MAIN_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Main_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Main_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main_GUI

% Last Modified by GUIDE v2.5 09-Dec-2018 18:07:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main_GUI is made visible.
function Main_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Main_GUI (see VARARGIN)

% Choose default command line output for Main_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Main_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Main_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function Num_ODE_Callback(hObject, eventdata, handles)
% hObject    handle to Num_ODE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Num_ODE = str2double(get(hObject,'String'));

guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Num_ODE as text
%        str2double(get(hObject,'String')) returns contents of Num_ODE as a double


% --- Executes during object creation, after setting all properties.
function Num_ODE_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Num_ODE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Num_algebric_equation_Callback(hObject, eventdata, handles)
% hObject    handle to Num_algebric_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Num_algebric_equation = str2double(get(hObject,'String'));
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Num_algebric_equation as text
%        str2double(get(hObject,'String')) returns contents of Num_algebric_equation as a double


% --- Executes during object creation, after setting all properties.
function Num_algebric_equation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Num_algebric_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_Add_equations.
function pushbutton_Add_equations_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Add_equations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Init_value = []
for ii = 1:handles.Num_ODE
    Add_ODE()
    uiwait
    load('Numerator.mat', 'Numerator');
    handles.Numerator{ii} = Numerator
    load('Denominator.mat', 'Denominator');
    handles.Denominator{ii} = Denominator
    load('Right_Hand_Side.mat', 'Right_Hand_Side');
    handles.Right_Hand_Side{ii} = Right_Hand_Side
    load('Init_value.mat', 'Init_value');
    handles.Init_value = [handles.Init_value,Init_value]
    handles.Eq_ODE{ii} = strcat('d', Numerator,'/d', Denominator, ' = ', Right_Hand_Side, '  . ',Numerator, '(0)', num2str(Init_value));
%     disp(handles.Eq_ODE{ii})
end

for jj = 1:handles.Num_algebric_equation
    Add_Algebric_equation()
    uiwait
    load('Algebric_equation.mat', 'algebric_equation');    
    handles.algebric_equation {jj} = algebric_equation
    disp(handles.algebric_equation {jj})
end
disp('All equations are correctlly set');
set(handles.listbox_Added_ODE,'string',handles.Eq_ODE);
set(handles.listbox_Added_alg_eq,'string',handles.algebric_equation); 
guidata(hObject, handles);


% --- Executes on selection change in listbox_Added_ODE.
function listbox_Added_ODE_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_Added_ODE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 
%  set(handles.listbox_Added_ODE,'max',length(LboxNames));
%  get(handles.listbox1,'value')
% Hints: contents = cellstr(get(hObject,'String')) returns listbox_Added_ODE contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_Added_ODE


% --- Executes during object creation, after setting all properties.
function listbox_Added_ODE_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_Added_ODE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox_Added_alg_eq.
function listbox_Added_alg_eq_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_Added_alg_eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_Added_alg_eq contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_Added_alg_eq


% --- Executes during object creation, after setting all properties.
function listbox_Added_alg_eq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_Added_alg_eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_Correct_ODE.
function pushbutton_Correct_ODE_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Correct_ODE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
A = get(handles.listbox_Added_ODE,'value');
% disp(get(handles.listbox_Added_ODE,'value'));
Index_1 = A(1);
% Check if the user add a new equation
load('Numerator.mat', 'Numerator');
old_Numerator = Numerator;
load('Denominator.mat', 'Denominator');
old_Denominator = Denominator;
load('Right_Hand_Side.mat', 'Right_Hand_Side');
old_Right_Hand_Side = Right_Hand_Side;
load('Init_value.mat', 'Init_value');
old_Init_value = Init_value;
%
Add_ODE()
uiwait
% Check if the user put new values
load('Numerator.mat', 'Numerator');
load('Denominator.mat', 'Denominator');
load('Right_Hand_Side.mat', 'Right_Hand_Side');
load('Init_value.mat', 'Init_value');
if isequal(Numerator, old_Numerator) && isequal(Denominator, old_Denominator) && isequal(Right_Hand_Side, old_Right_Hand_Side) && isequal(Init_value, old_Init_value)
    % The user don't add a new equation. Nothing is done.
else
    % The user add a new equation. It is saved.
    handles.Numerator {Index_1} = Numerator
    handles.Denominator {Index_1} = Denominator
    handles.Right_Hand_Side {Index_1} = Right_Hand_Side
    handles.Init_value (Index_1) = Init_value
    handles.Eq_ODE {Index_1} = ['d', num2str(Numerator),'/d', num2str(Denominator), ' = ', num2str(Right_Hand_Side), '. ',num2str(Numerator), '(0)'];
    set(handles.listbox_Added_ODE,'string',handles.Eq_ODE);
    guidata(hObject, handles);
end

guidata(hObject, handles);


% --- Executes on button press in pushbutton_Correct_alg_eq.
function pushbutton_Correct_alg_eq_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Correct_alg_eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.listbox_Added_alg_eq,'string',handles.algebric_equation); 
B = get(handles.listbox_Added_alg_eq,'value');
Index_2 = B(1);
% Check if the user enter a new value
load('Algebric_equation.mat', 'algebric_equation');
old_algebric_equation = algebric_equation;
%
Add_Algebric_equation();
uiwait;
load('Algebric_equation.mat', 'algebric_equation');
if isequal(algebric_equation,old_algebric_equation)
    % The user don't add a new equation. Nothing is done.
else
    % The user add a new equation. It is saved.
    handles.algebric_equation {Index_2} = algebric_equation;
    set(handles.listbox_Added_alg_eq,'string',handles.algebric_equation);
end



% --- Executes on button press in pushbutton_Solve_and_plot.
function pushbutton_Solve_and_plot_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Solve_and_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Numerator = handles.Numerator;
Denominator = handles.Denominator;
Right_Hand_Side = handles.Right_Hand_Side;
algebric_equation = handles.algebric_equation ;
Init_value = handles.Init_value;
Num_ODE = handles.Num_ODE;
Lower_value = handles.Lower_value;
Higher_value = handles.Higher_value;
% numerators = str2num(cell2mat(Numerator))
% disp(Numerator(1))
% y_output = Solver(Numerator, Denominator, Right_Hand_Side, algebric_equation, Init_value,Num_ODE, Lower_value,Higher_value)
% disp(y_output(:))
tspan = linspace(Lower_value, Higher_value);
% handles;
[t, y] = ode45(@(t,y) ODE(t, y, Denominator, Numerator, Right_Hand_Side, algebric_equation),tspan, Init_value);
handles.t = t
handles.y = y
guidata(hObject, handles);



% --- Executes on button press in pushbutton_Export_results.
function pushbutton_Export_results_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Export_results (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% [file,path,indx] = uiputfile('Ode rzsults');
t = handles.t;
y = handles.y;
filter = {'*.csv';'*.xls';'*.xlsx';'*.mat'};
[file, path, ext] = uiputfile(filter);
if ext == 2
    xlswrite([path, '/', file],[t(:);y(:)]);
elseif ext == 3
    xlswrite([path, '/', file],[t(:);y(:)]);
elseif ext == 1
    csvwrite([path, '/', file],{t,y});
elseif ext == 4
    save([path, '/', file],'t','y');
end

function Lower_value_Callback(hObject, eventdata, handles)
% hObject    handle to Lower_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Lower_value = str2double(get(hObject,'String'));
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Lower_value as text
%        str2double(get(hObject,'String')) returns contents of Lower_value as a double


% --- Executes during object creation, after setting all properties.
function Lower_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Lower_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Higher_value_Callback(hObject, eventdata, handles)
% hObject    handle to Higher_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Higher_value = str2double(get(hObject,'String'));
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Higher_value as text
%        str2double(get(hObject,'String')) returns contents of Higher_value as a double


% --- Executes during object creation, after setting all properties.
function Higher_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Higher_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Steep_value_Callback(hObject, eventdata, handles)
% hObject    handle to Steep_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Steep_value = str2double(get(hObject,'String'));
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Steep_value as text
%        str2double(get(hObject,'String')) returns contents of Steep_value as a double


% --- Executes during object creation, after setting all properties.
function Steep_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Steep_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% Additionals functions
function dydt = ODE(t, y, denominators, numerators, RHSs, explicitEqns)
% Input: t and y are the independent and dependent variable values
% denominators, numerators, RHSs, and explicitEqns are cell
% arrays with the first three terms defining the ODEs of the form
% d(numerators) / d(denominators) = RHSs and the fourth term
% defining the associated explicit equations
% Output: the derivative vector dy/dt for y(1):y(numODEs) where
% numODEs = length(numerators) = length(RHSs) = length(denominators)
% independent variable
str0=cell2mat(denominators(1));
eval(strcat(str0,'=t;'));
% dependent variables
for i = 1:length(numerators)
str1 = cell2mat(numerators(i));
eval(strcat(str1,'=y(i);'));
% str2func(strcat('@',str1))=y(i);
end
% explicit equations provided in MATLAB acceptable order, can be semicolon separated list
for i = 1:length(explicitEqns)
str2 = cell2mat(explicitEqns(i));
eval(str2);
end
% Right-hand sides of ODE definitions
for i = 1:length(RHSs)
%     A = RHSs(i);
%     A = cell2mat(A);
%     str3 = A;
%     dydt(i) = str3;
str3 = cell2mat(RHSs(i));
dydt(i) = eval(str3);
end
dydt = dydt';

% function y_output = Solver(Numerator, Denominator, Right_Hand_Side, algebric_equation, Init_value,Num_ODE, Lower_value,Higher_value)
% disp('Num_ODE')
% disp(Num_ODE)
% 
% % for i = 1:Num_ODE;
%     tspan = linspace(Lower_value, Higher_value);
%     [~,y_calc] = ode45(@(t,y) ODE(t, y, Numerator(i), Denominator(i), Right_Hand_Side(i), algebric_equation),tspan, Init_value(i));
%     y_output(i,:)=y_calc;
%     disp(['i = ', num2str(i)]);
% % end


% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes1


% --- Executes on button press in pushbutton_plot.
function pushbutton_plot_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

t = handles.t;
y = handles.y;
leg = '';
hold on
title('Solution of the ODE');
xlabel(handles.Denominator(1));
ylabel(handles.Numerator);
axes(handles.axes1)
cla;
for ii=1:handles.Num_ODE
    leg {end+1} = ['Equation # ', num2str(ii)]; 
end
plot(t,y)
legend(leg(:) )
hold off


% --- Executes during object creation, after setting all properties.
function pushbutton_plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushbutton_plot_selcted_eq.
function pushbutton_plot_selcted_eq_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_plot_selcted_eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% get(handles.listbox_Added_alg_eq,'string',handles.algebric_equation); 
B = get(handles.listbox_Added_ODE,'value')
Index = B(:);
t = handles.t;
y = handles.y;
leg = '';
hold on
title('Solution of the ODE');
xlabel(handles.Denominator(1));
ylabel(handles.Numerator);
axes(handles.axes1)
cla;
% for ii=1:handles.Num_ODE
%     leg {end+1} = ['Equation # ', num2str(ii)]; 
% end
for ii = 1:length(Index)
    leg {end+1} = ['Equation # ', num2str(Index(ii))];
    plot(t,y(Index(ii)))
end
legend(leg(:) )
hold off


% --- Executes on button press in pushbutton_export_plot.
function pushbutton_export_plot_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_export_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
figure_handle = isolate_axes(handles.axes1)
filter = {'*.png';'*.jpeg';'*pdf'};
[file, path, ext] = uiputfile(filter);
if ext == 1
    export_fig(figure_handle,[path, '/', file, '.png']);
elseif ext == 2
    export_fig(figure_handle,[path, '/', file, '.jpeg']);
elseif ext ==3
    export_fig(figure_handle,[path, '/', file, '.pdf']);
end
