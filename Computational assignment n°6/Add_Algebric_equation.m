function varargout = Add_Algebric_equation(varargin)
% ADD_ALGEBRIC_EQUATION MATLAB code for Add_Algebric_equation.fig
%      ADD_ALGEBRIC_EQUATION, by itself, creates a new ADD_ALGEBRIC_EQUATION or raises the existing
%      singleton*.
%
%      H = ADD_ALGEBRIC_EQUATION returns the handle to a new ADD_ALGEBRIC_EQUATION or the handle to
%      the existing singleton*.
%
%      ADD_ALGEBRIC_EQUATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ADD_ALGEBRIC_EQUATION.M with the given input arguments.
%
%      ADD_ALGEBRIC_EQUATION('Property','Value',...) creates a new ADD_ALGEBRIC_EQUATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Add_Algebric_equation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Add_Algebric_equation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Add_Algebric_equation

% Last Modified by GUIDE v2.5 08-Dec-2018 13:47:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Add_Algebric_equation_OpeningFcn, ...
                   'gui_OutputFcn',  @Add_Algebric_equation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Add_Algebric_equation is made visible.
function Add_Algebric_equation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Add_Algebric_equation (see VARARGIN)

% Choose default command line output for Add_Algebric_equation
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Add_Algebric_equation wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Add_Algebric_equation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function Insert_algebric_equation_Callback(hObject, eventdata, handles)
% hObject    handle to Insert_algebric_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.algebric_equation = get(hObject,'String');
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Insert_algebric_equation as text
%        str2double(get(hObject,'String')) returns contents of Insert_algebric_equation as a double


% --- Executes during object creation, after setting all properties.
function Insert_algebric_equation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Insert_algebric_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_Add_algebraic_equation.
function pushbutton_Add_algebraic_equation_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Add_algebraic_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
algebric_equation = handles.algebric_equation;
save('Algebric_equation.mat', 'algebric_equation');
guidata(hObject, handles);
close