\documentclass[12pt]{article}

\title{Computational Assignment}
\author{Rajendra Maharjan}
\date{\today}

\usepackage{graphicx}
\usepackage[noabbrev]{cleveref}
\bibliographystyle{abbrv}

\begin{document}
\maketitle

\tableofcontents
\newpage

\section{Ice Nucleation}\label{sec:section1}
\paragraph{}
An \textbf{Ice nucleus} is a particle which acts as the nucleus for the formation of an ice crystal in the atmosphere. One obvious example is the formation of ice, which influences global phenomena such as climate change, as well as processes happening at the nanoscale, such as intracellular freezing. On the other hand, controlling nucleation of molecular crystals from solutions is of great importance to pharmaceuticals, particularly in the context of drug design and production. Each of the above scenarios starts from a liquid below its melting temperature which is called \textbf{Supercooled Liquid} \cite{Principle_MetLiq}.

\subsection{Classical Nucleation Theory}
\paragraph{}
Almost every computer simulation of crystal nucleation in liquids invokes some elements of \textit{Classical Nucleation Theory} (\textbf{\textit{CNT}}). \textbf{\textit{CNT}} helps to build up the concept that can also be applied to the crystallization of supercooled liquids and supersaturated solutions. According to the capillary approximation, the interplay between the interfacial free energy, $\gamma_s$, and the difference in free energy between the liquid and the crystal, $\Delta$$\mu_v$, fully describes the thermodynamics of crystal nucleation. In 3-D, the free energy of formation, $\Delta$$G_N$, for a spherical crystalline nucleus of radius r is given by \cref{eqn:equation1} in \cref{sec:section1};

\begin{equation}
\label{eqn:equation1}
\Delta G_N = \underbrace{4 \pi r^2 \gamma_s}_{\mbox{surface term}} - \underbrace{\frac{4 \pi}{3}r^3 \Delta \mu_v}_{\mbox{volume term}}
\end{equation}

\subsection{Two-Step Nucleation}
\paragraph{}
In the old age of \textbf{\textit{CNT}}, it is no surprise that substantial efforts have been devoted to extend and/or improve its original theoretical framework. The most relevant modifications possibly concern the issue of two-step nucleation.

In the original formulation of \textbf{\textit{CNT}}, the system has to overcome a single free energy barrier, corresponding to a crystalline nucleus of a certain critical size. In fact, the formation of crystals from molecules in solution often occurs according to a two-step nucleation mechanism that has no place in the original formulation of \textbf{\textit{CNT}}. In the prototypical scenario depicted, a first free energy barrier, $\Delta$$G_{n, two step}^*$ has to overcome by means of a density fluctuation of the solute, such that a cluster of connected molecules of size $n_p^*$ is formed.

\section{Water}\label{sec:section2}
\paragraph{}
The nucleation of crystals in water is one of nature's most ubiquitous phenomena. As the early stages of nucleation involve exceedingly small time and length scales, atomistic computer simulations can provide unique insights into the microscopic aspects of crystallization. The numerous molecular dynamics simulations have unraveled crucial aspects of crystal nucleation in liquids. \textbf{\textit{MD}} simulations have provided key insights into diverse nucleation scenarios, ranging from colloidal particles to natural gas hydrates, and that, as a result, the general applicability of \textbf{\textit{CNT}} has been doubtful. \textbf{\textit{MD}} simulation is given by \cref{eqn:equation2} in \cref{sec:section2}.

\begin{equation}
\frac{d}{dt}(\frac{\partial L}{\partial q_k}) - (\frac{\partial L}{\partial q_k}) = 0
\label{eqn:equation2}
\end{equation}

\subsection{Homogeneous Ice Nucleation}
\paragraph{}
Homogeneous ice nucleation is the nucleation phenomenon in which ice crystals grow from plain water without any impurities like salts, ions etc. Experimentally, water begins to crystallize at 273.15K, this one is referred to as heterogeneous nucleation. For homogeneous nucleation, the nucleation temperature is even lower at around 240K \cite{matsumoto_paper}.
There are many water model system that can be used to carry out simulation of Ice and have its own importance. The water models I am using are;

\begin{enumerate}
\item TIP4P	: TP4
\item TIP4P-Ew : T4E
\item TIP4P/2005 : T45 
\item TIP4P/Ice : T4I
\end{enumerate}

I carried out the following simulation experiment using TIP4P/Ice in which the dimension of the original box was decreased by 2 percent and the fastest nucleation rate occurred at 240K which was 57ns. However, at other temperatures above or below 240K took even longer time for the nucleation.
The table for the simulation at different temperature in 2\% contraction and its corresponding figure for crystal growth at 240K is given below in \cref{tab:table1} and \cref{fig:figure1} in \cref{sec:section2} respectively.

\begin{table}[!ht]
  \begin{center}
    \caption{\textbf{Simulation at 0.98 extension.}}
    \label{tab:table1}
    \begin{tabular}{|c|c|c|c|c|}
      \hline
      \multicolumn{5}{|c|}{\textbf{Extension = 0.98}}\\
      \hline
      \textbf{S.N.} & \textbf{210} & \textbf{220} & \textbf{230} & \textbf{240}\\
      \hline
      1 & 483.66 & 142.565 & 48.6111 & 54.7386\\
      2 &  & 105.801 & 90.482 & 84.3546\\
      3 & 491/83 & 174.224 & 63.9297 & 55.7598\\
      4 & 273.284 & 152.778 & 46.5686 & 54.7386\\
      5 &  & 145.629 & 114.992 & 34.3137\\
      \hline
      \textbf{Average (ns)} & 416 & 144 & 73 & \textbf{57}\\
      \hline
      \textbf{Std Error (ns)} & 72 & 11 & 13 & \textbf{8}\\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\begin{figure}[!ht]
  \begin{center}
 	 \includegraphics[width=0.8\textwidth]{confout.jpg}
  \end{center}
	 \caption{The hydrogen bond network structure of water at a given time in inherent structures at t = 57ns. Lines indicate hydrogen bonds among water molecules, and the intermolecular bonds of water participating in such hydrogen bonds. Red balls represent oxygen atoms and white balls represent hydrogen atoms.}
  \label{fig:figure1}
\end{figure}

\subsection{Heterogeneous Ice Nucleation}
\paragraph{}
As mentioned in the previous section, homogeneous ice nucleation becomes extremely slow at moderate supercooling. This seems at odds with our everyday experience - we do not, for example, have to wait for temperatures to reach $-30^o$C before we have to use a deicer on our car windows. In fact, the formation of ice in nature occurs almost exclusively heterogeneously, thanks to the presence of foreign particles. These ice-nucleation agents facilitate the formation of ice by lowering the free energy barrier for nucleation.

Heterogeneous nucleation is customarily formulated within the \textbf{\textit{CNT}} framework in terms of geometric arguments. Specifically given by \cref{eqn:equation3} in \cref{sec:section2};

\begin{equation}
\label{eqn:equation3}
\Delta G^*_N(heterogeneous) = \Delta G^*_N(homogeneous).f(\theta)
\end{equation}

where $f(\theta) \le$ 1 is the \textit{shape factor}, a quantity that accounts for the fact that three different interfacial free energies must be balanced. $G^*_N$ is the lowering of free energy barrier.

\newpage
\section{References}
\bibliography{bib_rajendra_maharjan}

\end{document}