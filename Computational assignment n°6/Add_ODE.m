function varargout = Add_ODE(varargin)
% ADD_ODE MATLAB code for Add_ODE.fig
%      ADD_ODE, by itself, creates a new ADD_ODE or raises the existing
%      singleton*.
%
%      H = ADD_ODE returns the handle to a new ADD_ODE or the handle to
%      the existing singleton*.
%
%      ADD_ODE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ADD_ODE.M with the given input arguments.
%
%      ADD_ODE('Property','Value',...) creates a new ADD_ODE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Add_ODE_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Add_ODE_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Add_ODE

% Last Modified by GUIDE v2.5 08-Dec-2018 13:19:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Add_ODE_OpeningFcn, ...
                   'gui_OutputFcn',  @Add_ODE_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Add_ODE is made visible.
function Add_ODE_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Add_ODE (see VARARGIN)

% Choose default command line output for Add_ODE
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Add_ODE wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Add_ODE_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function Numerator_Callback(hObject, eventdata, handles)
% hObject    handle to Numerator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Numerator = get(hObject,'String');
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Numerator as text
%        str2double(get(hObject,'String')) returns contents of Numerator as a double


% --- Executes during object creation, after setting all properties.
function Numerator_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Numerator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Denominator_Callback(hObject, eventdata, handles)
% hObject    handle to Denominator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Denominator = get(hObject,'String');
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Denominator as text
%        str2double(get(hObject,'String')) returns contents of Denominator as a double


% --- Executes during object creation, after setting all properties.
function Denominator_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Denominator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Right_Hand_Side_Callback(hObject, eventdata, handles)
% hObject    handle to Right_Hand_Side (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Right_Hand_Side = get(hObject,'String');
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Right_Hand_Side as text
%        str2double(get(hObject,'String')) returns contents of Right_Hand_Side as a double


% --- Executes during object creation, after setting all properties.
function Right_Hand_Side_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Right_Hand_Side (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Init_value_Callback(hObject, eventdata, handles)
% hObject    handle to Init_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Init_value = str2double(get(hObject,'String'));
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of Init_value as text
%        str2double(get(hObject,'String')) returns contents of Init_value as a double


% --- Executes during object creation, after setting all properties.
function Init_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Init_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_Add_ODE.
function pushbutton_Add_ODE_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Add_ODE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Numerator = handles.Numerator;
Denominator = handles.Denominator;
Right_Hand_Side = handles.Right_Hand_Side;
Init_value = handles.Init_value;

save('Numerator.mat', 'Numerator');
save('Denominator.mat', 'Denominator');
save('Right_Hand_Side.mat', 'Right_Hand_Side');
save('Init_value.mat', 'Init_value');
guidata(hObject, handles);
close
