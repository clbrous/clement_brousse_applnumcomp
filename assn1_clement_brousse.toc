\contentsline {section}{\numberline {1}Difficulties to weld railway}{2}
\contentsline {subsection}{\numberline {1.1}Difficulties to access}{2}
\contentsline {subsection}{\numberline {1.2}Difficulties to control the environment}{2}
\contentsline {section}{\numberline {2}The various welding method}{2}
\contentsline {subsection}{\numberline {2.1}The melt metal}{3}
\contentsline {subsection}{\numberline {2.2}The electrical process}{3}
\contentsline {section}{\numberline {3}The simulation process}{3}
\contentsline {subsection}{\numberline {3.1}The electrical process}{3}
\contentsline {subsection}{\numberline {3.2}The thermal process}{3}
\contentsline {section}{\numberline {4}The railroad around the world}{4}
