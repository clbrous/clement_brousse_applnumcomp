%   Computational assignment n�4 - Three-lump model (Matlab code)
%
%   This code is design to find a value for differential equations by
%   comparison to experimental data. The model study is the three lump
%   model.
%
%   @ Autor Clement Brousse (clbrous@okstate.edu)
%   This code has been partially adapted from exemples written by Dr Ford
%   Versypt at the OSU.
%
%   The equations solved are
%   $\frac{dy_1}{dt}=-(k_1+k_3)y1^2$
%   $\frac{dy_2}{dt}=k_1*y_1^2-k_2*y_2$
%   $\frac{dy_3}{dt}=k_3*y_1^2+k_2*y_2$
%
%   The initials conditions are the followings :
%   $y_1(0)=1$
%   $y_2(0)=0$
%   $y_3(0)=0$
%
%   The input values neededs as input are the experimental values and the
%   initial conditions.
%   This script return the values of $k_1$, $k_2$ and $k_3$.
%   This script also return the plot of the wheight fraction according 
%   to the time and the wheight fraction according to the conversion.
%
function param_estim_3lump
%% Define the values needed
% Experimental values
tdata = [1/60, 1/30, 1/20, 1/10];
cdata = [0.4926, 0.6204, 0.7118, 0.8238];
ydata = [0.5074, 0.3796, 0.2882, 0.1762; 0.3767, 0.4385, 0.4865, 0.5416; 0.0885+0.0274, 0.136+0.0459, 0.1681+0.0572, 0.2108+0.0714];    % y,; y2; y3

% Initial conditions
y0(1) = 1;
y0(2) = 0;
y0(3) = 0;

% Values tested for k1, k2 and k3 
k(1) = 1;
k(2) = 0;
k(3) = 0;
parameterguesses = k;

%% Estimate parameters
[parameters,resnorm,residuals,exitflag,] = lsqcurvefit(@(parameters,tdata) three_lump_sol(parameters,tdata,y0), parameterguesses,tdata,ydata);

%% Plot the results
% Plot the results vs Time
figure(1)
hold on
title('Weight fraction vs time for the three-lump model');
plot (tdata, ydata(1,:), 'rx');
plot (tdata, ydata(2,:), 'b*');
plot (tdata, ydata(3,:), 'g+');
xlabel('Time [h]');
ylabel('Weight fraction');
tforplotting =0.001:0.01:1;
y_calc = three_lump_sol(parameters,tforplotting,y0);
plot (tforplotting, y_calc(1,:), 'r-o');
plot (tforplotting, y_calc(2,:), 'b-o');
plot (tforplotting, y_calc(3,:), 'g-o');
axis([0 0.1 0 1]);
hold off
legend('VGO', 'Gasoline', 'Gas + Coke', 'VGO predicted', 'Gasoline predicted', 'Gas + Coke predicted');

% Plot the result vs conversion
figure(2)
hold on
title('Weight fraction vs conversion for the three-lump model');
plot (cdata, ydata(1,:), 'rx');
plot (cdata, ydata(2,:), 'b*');
plot (cdata, ydata(3,:), 'g+');
xlabel('Conversion');
ylabel('Weight fraction');
conv_calc = 1-y_calc(1,:);
plot (conv_calc, y_calc(1,:), 'r-o');
plot (conv_calc, y_calc(2,:), 'b-o');
plot (conv_calc, y_calc(3,:), 'g-o');
axis([0 1 0 1]);
hold off
legend('VGO', 'Gasoline', 'Gas + Coke', 'VGO predicted', 'Gasoline predicted', 'Gas + Coke predicted');


%% Define the equations
    function dydt = three_lump_eq(t,y,parameters)
        k(1) = parameters(1);
        k(2) = parameters(2);
        k(3) = parameters(3);
        dydt (1) = -(k(1)+k(3)).*y(1).^2;
        dydt (2) = k(1).*y(1).^2-k(2).*y(2);
        dydt (3) = k(3).*y(1).^2+k(2).*y(2);
        dydt = dydt';
    end
%% Define the solution
    function y_output = three_lump_sol(parameters, tdata, y0)
        for i = 1:length(tdata);
            tspan = linspace(0,tdata(i),101);
            [~,y_calc] = ode23s(@(t,y) three_lump_eq(t,y,parameters),tspan,y0);
            y_output(i,:)=y_calc(end,:);
         end        
        y_output = y_output';
    end

%% Show the solution
X = sprintf('The parameters found are : \n\t k_1 = %d \n\t k_2 = %d \n\t k_3 = %d',k(1),k(2),k(3));
disp(X)
end
