function output = system_of_ODEs(varargin)
% The possible variables are t,C,k1, k2, k3, k4
% The variables should be given in the right order
% The equations this program solve are 
% frac{\partial C_A}{\partial t} = - \k_1 \C_A - \k_2 \C_A
% frac{\partial C_B}{\partial t} =   \k_1 \C_A - \k_3 - \k_4 \C_B
% 
% $$frac{\partial C_A}{\partial t} = - \k_1 \C_A - \k_2 \C_A$$
% $$frac{\partial C_B}{\partial t} =   \k_1 \C_A - \k_3 - \k_4 \C_B$$ 
%
% Autor : Clement Brousse
% mail : clbrous@okstate.edu
% Last modification 09/09/2018
% Version 6
% If you find a mistake please contact the autor by mail
%
%Declare the defaults values

t=0;    % Variable useless in this program
C(1)=6.25;
C(2)=0;
k_1=0.15;
k_2=0.6;
k_3=0.1;
k_4=0.2;    

if nargin == 0
% Nothing to do the defaults values are even used.
%disp("Case input = 0"); %Only used to test the program

elseif nargin == 6
    k_1 = varargin(3);
    k_2 = varargin(4);
    k_3 = varargin(5);
    k_4 = varargin(6);
    C=varargin(2);
    %disp("Case input = 6"); %Only used to test the program
% Extract the k_i values in case there is 6 inputs.

elseif nargin>=2
    C=varargin(2);
    %disp("Case input = 2"); %Only used to test the program
% Extract the C_A and C_B values for the cas with 2 and 6 input.
end

%Compute the ODEs
ODE(1) = -k_1{1}*C{1,1}{1} -k_2{1}*C{1,1}{1};
ODE(2) = k_1{1}*C{1,1}{1} -k_3{1} -k_4{1}*C{1,1}{2};
% Show the results :
disp("dC_A/dt = " + ODE(1) + " [mg.l^-1.h^-1]")
disp("dc_B/dt = " + ODE(2) + " [mg.L^-1.h^-1]")