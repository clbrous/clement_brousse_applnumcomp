function output = system_of_ODEs(t,C,k_1,k_2,k_3,k_4)
%Check the variable type
if  isfloat(k_1)==1 && isfloat(k_2)==1 && isfloat(k_3)==1 && isfloat(k_4)==1 && isvector(C)==1
   % If the data type is good
   disp("Good data type !")
else disp("Error the data type is uncorrect.")
    disp("Please check the input data")
    disp("t,K1, K2, K3, K4 should be integer and C is a matrix")
    disp("The variables should be in the following order : t,C,K1,K2,K3,K4")
    %If the data type is uncorrect, the program pshow an error message
    return
    %Close the program
C_A=C(1,0)
C_B=C(0,1)
%create the ODE system
 diff(C_A(t), t) == -k_1*C_A -k_2*C_A
 diff(C_B(t), t) == k_1*C_A -k_3 -k_4*C_B
end
    